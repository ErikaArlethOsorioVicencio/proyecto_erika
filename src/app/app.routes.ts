
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ErrorComponent } from './pages/error/error.component';


//Rutas
const app_routes: Routes = [
    { path: 'inicio', component: HomeComponent},
    { path: 'acerca de', component: AboutComponent},
    { path: 'contacto', component: ContactComponent},
    {path: 'error', component: ErrorComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'error' }
];


export const app_routing = RouterModule.forRoot(app_routes);



